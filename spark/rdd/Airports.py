import sys

from pyspark import SparkConf, SparkContext


def main(file_name: str) -> None:

    spark_conf = SparkConf()
    spark_context = SparkContext(conf=spark_conf)

    logger = spark_context._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)


    airports = spark_context \
        .textFile(file_name)\
        .map(lambda line: line.split(",")) \
        .filter(lambda array: array[8] == "\"ES\"") \
        .map(lambda array: (array[2], 1))\
        .reduceByKey(lambda x, y: x + y)        \
        .sortBy(lambda pair: pair[1], False) \
        .persist()

    for line in airports.collect():
        print(line)

    spark_context.stop()



if __name__ == "__main__":
    """
    Python program that uses Apache Spark to sum a list of numbers stored in files
    """

    if len(sys.argv) != 2:
        print("Usage: spark-submit AddNumbersFromFilesWithTime.py <file>", file=sys.stderr)
        exit(-1)

    main(sys.argv[1])

