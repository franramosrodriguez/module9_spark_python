from pyspark.sql import SparkSession, functions
import time


def main() -> None:

    spark_session = SparkSession \
        .builder \
        .getOrCreate()

    start_computing_time = time.time()

    logger = spark_session._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    data_frame = spark_session \
        .read \
        .format("csv") \
        .options(header='true', inferschema='true') \
        .load("data/airports.csv") \

    data_frame.filter(data_frame["iso_country"].contains("ES")) \
        .groupBy("type") \
        .count() \
        .sort("count", ascending=False).show()

    total_computing_time = time.time() - start_computing_time
    print("Computing time: ", str(total_computing_time))


if __name__ == "__main__":
    """
    Python program that uses Apache Spark to sum a list of numbers stored in files
    """


    main()