from pyspark.sql import SparkSession, functions
import time


def main() -> None:

    spark_session = SparkSession \
        .builder \
        .getOrCreate()

    start_computing_time = time.time()

    logger = spark_session._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    airport = spark_session \
        .read \
        .format("csv") \
        .options(header='true', inferschema='true') \
        .load("data/airports.csv")

    print("Aeropuertos")
    airport.printSchema()
    airport.show()

    large_airport= airport\
        .filter(airport["type"].contains("large_airport"))\
        .groupBy("iso_country")\
        .count()

    large_airport.printSchema()
    large_airport.show()

    countries = spark_session\
        .read.options(header="true", inferschema = "true")\
        .csv("data/countries.csv")

    print("Paises")
    countries.printSchema()
    countries.show()

    # Join the dataframes
    join_dataframe = large_airport\
        .join(countries.withColumnRenamed("code", "iso_country"), ["iso_country"])\
        .select("name", "count")\
        .sort("count", ascending=False)

    print("Join")
    join_dataframe.printSchema()
    join_dataframe.show()

    total_computing_time = time.time() - start_computing_time
    print("Computing time: ", str(total_computing_time))


if __name__ == "__main__":
    """
    Python program that uses Apache Spark to sum a list of numbers stored in files
    """


    main()