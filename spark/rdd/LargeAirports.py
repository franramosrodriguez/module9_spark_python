import sys

from pyspark import SparkConf, SparkContext


def main() -> None:

    spark_conf = SparkConf()
    spark_context = SparkContext(conf=spark_conf)

    logger = spark_context._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    #Get airports RDD
    airports = spark_context \
        .textFile("data/airports.csv")\
        .map(lambda line: line.split(",")) \
        .filter(lambda list: list[2] == "\"large_airport\"") \
        .map(lambda list: (list[8], 1))\
        .reduceByKey(lambda x, y: x + y) \

    #Get countries RDD
    countries = spark_context\
        .textFile("data/countries.csv") \
        .map(lambda line: line.split(",")) \
        .map(lambda list: (list[1], list[2]))

    result= airports\
        .join(countries) \
        .map(lambda pair: (pair[1][1], pair[1][0])) \
        .sortBy(lambda pair: pair[1], ascending=False)


    for record in result.take(20):
        print(record)

    spark_context.stop()

if __name__ == "__main__":
    """
    Python program that uses Apache Spark to sum a list of numbers stored in files
    """


    main()
