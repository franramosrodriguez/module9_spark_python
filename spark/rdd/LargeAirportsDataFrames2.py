from pyspark.sql import SparkSession

import time


if __name__ == "__main__":
    spark_session = SparkSession \
        .builder \
        .getOrCreate()

    logger = spark_session._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    start_computing_time = time.time()

    # Get airports dataframe
    airports = spark_session\
        .read\
        .options(header='true', inferschema='true')\
        .csv("data/airports.csv")

    airports.printSchema()
    #airports.show()

    large_airports = airports\
        .filter(airports["type"].contains("large_airport")) \
        .groupBy("iso_country") \
        .count()

    large_airports.printSchema()
    large_airports.show()

    # Get airport name dataframe
    countries = spark_session\
        .read\
        .options(header='true', inferschema='true')\
        .csv("data/countries.csv")

    countries.printSchema()
    countries.show()

    # Join the dataframes
    join_dataframe = large_airports\
        .join(countries.withColumnRenamed("code", "iso_country"), ["iso_country"])\
        .select("name", "count")\
        .sort("count", ascending=False)

    join_dataframe.printSchema()
    join_dataframe.show()

    total_computing_time = time.time() - start_computing_time
    print("Computing time: ", str(total_computing_time))


