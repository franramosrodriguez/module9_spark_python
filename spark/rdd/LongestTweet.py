from pyspark import SparkConf, SparkContext
import sys

if __name__ == "__main__":
    if len(sys.argv)!=2:
        print("Usage: spark-submit MostActivateUser.py <file>", file=sys.stderr)
        exit(-1)

    spark_conf= SparkConf()
    spark_context = SparkContext(conf=spark_conf)

    logger = spark_context._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)



    # Find the longest tweet emitted by any user.
    # The program will print in the screen the user name, the length of the tweet, and the time and date of the tweet.

    output = spark_context \
        .textFile(sys.argv[1]) \
        .map(lambda line: line.split('\t')) \
        .map(lambda word: ( word[1],len(word[2]), word[3])) \
        .sortBy(lambda pair: pair[1], ascending=False) \
        .take(1)

    print("The longest Tweet:")

    for word in output:
        print("\nlength: "+str(word[1])+ "\nUser: " + str(word[0]) + "\nDateTime: " + str(word[2]))

        text_file = open("output3.txt", "w")
        text_file.write("The longest Tweet:\n")

        for word in output:
            text_file.write(str(word) + "\n")