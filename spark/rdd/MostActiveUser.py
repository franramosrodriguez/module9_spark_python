from pyspark import SparkConf, SparkContext
import sys




if __name__ == "__main__":
    if len(sys.argv)!=2:
        print("Usage: spark-submit MostActivateUser.py <file>", file=sys.stderr)
        exit(-1)

    spark_conf= SparkConf()
    spark_context = SparkContext(conf=spark_conf)

    logger = spark_context._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    #Find the user that has written the largest amount of tweets.
    # The program will print in the screen the name of the user and the number of tweets.

    user = spark_context \
        .textFile(sys.argv[1]) \
        .map(lambda line: line.split('\t')[1]) \
        .flatMap(lambda line: line.split(' ')) \
        .map(lambda word: (word, 1)) \
        .reduceByKey(lambda a, b: a + b) \
        .sortBy(lambda pair: pair[1], ascending=False) \
        .take(1)

    for word in user:
        print("\nThe most activate user: " +str(word))

    text_file = open("output2.txt", "w")
    text_file.write("The most activate user:\t")

    for word in user:
        text_file.write(str(word) + "\n")

    text_file.close()






    spark_context.stop()


