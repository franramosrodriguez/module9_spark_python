from pyspark import SparkConf, SparkContext
from nltk.corpus import stopwords
import sys

if __name__ == "__main__":
    if len(sys.argv)!=2:
        print("Usage: spark-submit TrendingTopic.py <file>", file=sys.stderr)
        exit(-1)

    spark_conf= SparkConf()
    spark_context = SparkContext(conf=spark_conf)

    logger = spark_context._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    #Trending Topic
    #Search for the 10 most repeated words in the matter field of the tweets.
    # The output of the program must be a file containing the words and their frequency, ordered by frequencey.
    # Note that common words (e.g. "RT", "http", ...) should be filtered.

    operators = ['RT', '-']
    stop_words = set(stopwords.words('english') + operators)

    output = spark_context \
        .textFile(sys.argv[1]) \
        .map(lambda line: line.split('\t')[2]) \
        .flatMap(lambda line: line.split(' ')) \
        .filter(lambda word: word not in stop_words) \
        .map(lambda word: (word, 1)) \
        .reduceByKey(lambda a, b: a + b) \
        .sortBy(lambda pair: pair[1], ascending=False) \
        .take(10)



    print("\nTrending Topic\n")

    print("The 10 most repeated words \n")

    for word in output:
        print(str(word))

    text_file = open("output1.txt", "w")
    text_file.write("The 10 most repeated words \n")


    for word in output:
        text_file.write(str(word)+ "\n")

    text_file.close()

    spark_context.stop()