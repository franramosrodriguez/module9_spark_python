from pyspark.sql import SparkSession
from pyspark.sql import functions

"""
Name,Age,Weight,HasACar,DateBirth
Luis,23,84.5,TRUE,
Lola,42,70.2,false
Paco,66,90.1,False
"""


def main() -> None:
    spark_session = SparkSession \
        .builder \
        .getOrCreate()

    logger = spark_session._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    data_frame = spark_session \
        .read \
        .format("csv") \
        .options(header='true', inferschema='true') \
        .load("data/simple.csv") \
        .persist()

    data_frame.printSchema()
    data_frame.show()

    # Print the data types of the data frame
    print("data types: " + str(data_frame.dtypes))

    # Describe the dataframe
    data_frame \
        .describe() \
        .show()

    # Select everything
    data_frame.select("Name").show()

    # Select columns name and age, but adding 1 to age
    data_frame.select("name", data_frame["Age"] > 1).show()

    # Select the rows having a name length > 4
    data_frame.select(functions.length(data_frame["Name"]) > 4).show()

    # Select names staring with L
    data_frame.select("name", data_frame["name"].startswith("L")).show()

    # Add a new column "Senior" containing true if the person age is > 45
    data_frame.withColumn("Senior", data_frame["Age"] > 45).show()

    # Rename column HasACar as Owner
    data_frame.withColumnRenamed("HasACar", "Owner").show()

    # Remove column DateBirth
    data_frame.drop("DateBirth").show()

    # Get a RDD
    rdd_from_dataframe = data_frame \
        .rdd \
        .cache()

    for i in rdd_from_dataframe.collect():
        print(i)

    # Sum all the weights (RDD)
    sum_of_weights = rdd_from_dataframe \
        .map(lambda row: row[2]) \
        .reduce(lambda x, y: x + y)  # sum()
    print("Sum of weights (RDDs): " + str(sum_of_weights))

    v = data_frame.select("Weight").groupBy().sum().collect()
    print(v[0][0])

    # Sum all the weights (dataframe)

    data_frame.select(functions.sum(data_frame["Weight"])).show()
    data_frame.agg({"Weight": "sum"}).show()

    # Get the mean age (RDD)
    mean_age = rdd_from_dataframe \
        .map(lambda row: row[1]) \
        .reduce(lambda x, y: x + y) / rdd_from_dataframe.count()

    print("Mean age (RDDs): " + str(mean_age))

    # Get the mean age (dataframe)
    data_frame.select(functions.avg(data_frame["Weight"])).show()
    data_frame.agg({"Weight": "avg"}).show()

    # Write to a json file
    data_frame \
        .write \
        .save("output.json", format="json")

    # Write to a CSV file
    data_frame \
        .write \
        .format("csv") \
        .save("output.csv")


if __name__ == '__main__':
    main()