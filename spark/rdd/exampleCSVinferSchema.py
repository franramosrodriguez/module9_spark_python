from pyspark.sql import SparkSession, functions


def main() -> None:

    spark_session = SparkSession \
        .builder \
        .getOrCreate()

    logger = spark_session._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    data_frame = spark_session \
        .read \
        .format("csv") \
        .options(header='true', inferschema='true') \
        .load("data/simple.csv") \
        .persist()

    data_frame.printSchema()
    data_frame.show()

    #Print the data types of the data frame
    print("data types: "+ str(data_frame.dtypes))

    data_frame.describe().show()

    #Salect everything
    data_frame.select("Name").show()

    #Select columns name and age, but adding 1 to age
    data_frame.select("name", data_frame["Age"] +1).show()


    data_frame.withColumn("Senior", data_frame["Age"]>45).show()

    #select names starting with L
    data_frame.select("name", data_frame["name"].startswith("L"))\
        .withColumnRenamed("starwith(name, L)", "L").show()

    #Remove column DateBirth
    data_frame.drop("DateBirth").show()

    #Get a RDD
    rdd_from_dataframe = data_frame\
        .rdd.cache()

    for i in rdd_from_dataframe.collect():
        print(i)

    #Sum all the Weights (RDD)
    sum_of_weights = rdd_from_dataframe \
        .map(lambda row: row[2]) \
        .reduce(lambda x, y: x+y) #sum()

    print("Sum of weights (RDDs): " +str(sum_of_weights))

    #Sum all the weights (dataframe)
    v = data_frame.select("Weight").groupBy().sum().collect()
    print(v[0][0])

    data_frame.select(functions.sum(data_frame["Weight"])).show()
    data_frame.agg({"Weight": "sum"}).show()

if __name__ == '__main__':
    main()
