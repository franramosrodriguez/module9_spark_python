from pyspark.sql import SparkSession


def main() -> None:

    spark_session = SparkSession \
        .builder \
        .getOrCreate()

    logger = spark_session._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    data_frame = spark_session \
        .read \
        .json("data/primer-dataset.json") \


    data_frame.printSchema()
    data_frame.show()

    #selecciona los nombres y tipo de cocina de todos los restaurantes (columnas name y cuisine)
    data_frame.select("name", "cuisine").show()

    #Filtra aquellos restaurantes de cocina tipo americana
    print("Filtra aquellos restaurantes de cocina tipo americana")
    data_frame.filter(data_frame["cuisine"] == "American").show()

    #Agrupa los restaurantes por barrio y los suma.
    print("Agrupa los restaurantes por barrio y los suma.")
    data_frame.groupBy("borough")\
        .count()\
        .show()

    data_frame.createOrReplaceGlobalTempView("restaurants")

    sql_data_frame = spark_session.sql("select * from restaurants").show()



if __name__ == '__main__':
    main()
