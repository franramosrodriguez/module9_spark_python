from pyspark import SparkConf, SparkContext
import sys

if __name__ == "__main__":
    if len(sys.argv)!=2:
        print("Usage: spark-submit passwdAnalysis.py <file>", file=sys.stderr)
        exit(-1)

    spark_conf= SparkConf()
    spark_context = SparkContext(conf=spark_conf)

    logger = spark_context._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.WARN)

    # Primera parte. Numero de usuarios
    salida = spark_context \
        .textFile(sys.argv[1]) \
        .cache()

    num_users = salida.count()


    print("\n--First part-- ")
    print("The number of user is: " + str(num_users) + "\n")

    # Segunda parte
    # Sort the users by user name and print the five first users, indicating only their name and their UID and GID.
    #Example; yarn:*:345:535:Hadoop Yarn:/var/lib/hadoop-yarn:/bin/bash


    output = spark_context \
        .textFile(sys.argv[1]) \
        .map(lambda line: line.split(':')) \
        .sortBy(lambda pair: pair[1], ascending=True) \
        .take(5)

    print("--Second part-- ")
    print("--First 5 users by Name, UID and GID--")
    for word in output:
        print("-User: "+ str(word[0]) + " -UID: "+ str(word[2]) + " -GID: "+ str(word[3]))


    # Tercera parte
    # Print the number of users having the command "/bin/bash" as last field

    output2 = spark_context \
        .textFile(sys.argv[1]) \
        .map(lambda line: line.split('/')) \
        .filter(lambda array: array[3] == "bin") \
        .filter(lambda array: array[4] == "bash") \
        .sortBy(lambda pair: pair[1], ascending=True) \

    num_users2 = output2.count()


    print("\n--Third part-- ")
    print("--Print the number of users having the command /bin/bash as last field--")

    print("The number of user is: " + str(num_users2) + "\n")

    spark_context.stop()